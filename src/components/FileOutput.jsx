import {
    makeStyles, Paper, TextField, Typography
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    output: {
        fontFamily: 'monospace',
        whiteSpace: 'pre',
        color: theme.palette.text.primary,
    }
}));


export default function FileOutput() {
    const classes = useStyles();

    const { output } = useGlobal();

    return (
        <Paper className={classes.root}>
            <Typography variant={'h5'} gutterBottom>Output</Typography>
            <TextField
                fullWidth
                disabled
                value={output}
                multiline
                rows={30}
                variant={'outlined'}
                inputProps={{className: classes.output}}
            />
        </Paper>
    );
}

