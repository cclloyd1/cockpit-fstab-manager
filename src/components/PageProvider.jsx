import React, {useContext, useEffect, useState} from 'react';
import {useToasts} from "react-toast-notifications";


const PageContext = React.createContext({});

const cockpit = window.cockpit;

export default function PageProvider(props) {
    const { children } = props;
    const { addToast } = useToasts();

    const [sections, setSections] = useState([]);
    const [output, setOutput] = useState('');
    const [beautify, setBeautify] = useState(true);
    const [maxBeautify, setMaxBeautify] = useState(48);
    const [autoUpdate, setAutoUpdate] = useState(true);

    // Auto Update Effect
    useEffect(() => {
        if (autoUpdate)
            genOutput();
    }, [sections]);

    const doesFileExist = (path) => {
        const file = cockpit.file(path, {max_read_size: 1});
        return file.read().then(() => {
            return true;
        }).catch(err => {
            return err.problem === 'too-large';
        })
    }

    const parseFstab = (content) => {
        let lines = content.split('\n');
        let sections = [];
        for (let line of lines) {
            if (line.length === 0)
                continue;


            if (line.startsWith('#')) {
                // Ignore other comments
                if (!line.startsWith('#!#'))
                    continue;
                // Add section to list
                const m = line.match(/^#!#\s(.+)$/)
                if (m === null) return;
                sections.push({
                    name: m[1],
                    mounts: [],
                });
            }
            else {
                let parts = line.match(/((?:\\.|\S)+)/g);
                if (parts) {
                    // Add default section if no sections found before mounts
                    if (sections.length === 0)
                        sections.push({
                            name: 'Default',
                            mounts: [],
                        });

                    sections[sections.length - 1].mounts.push({
                        device: parts[0],
                        mountpoint: parts[1],
                        fstype: parts[2],
                        options: parts[3],
                        dump: parts[4],
                        pass: parts[5],
                    });
                }
            }
        }
        console.log(sections);
        setSections([...sections]);
        return sections;
    }

    const handleAutoMount = () => {

    }

    const copyFile = (source, dest) => {
        const orig = cockpit.file(source, {superuser: 'require'});
        const backup = cockpit.file(dest, {superuser: 'require'});
        orig.read().then(content => {
            backup.replace(content).catch(err => {
                addToast('Error writing destination file', {appearance: 'error'})
            })
        }).catch(err => {
            addToast('Error reading source file', {appearance: 'error'})
        })
    }

    const handleSave = () => {
        const output = genOutput();

        if (!cockpit) {
            console.error('Cockpit not defined');
            addToast(`Cockpit not defined`, {appearance: 'error'});
            return;
        }

        // Generate backups
        const exists = doesFileExist('/etc/fstab')
        if (!exists) {
            console.log('fstab backup doesnt exist');
            copyFile('/etc/fstab', '/etc/fstab.orig');
        }
        copyFile('/etc/fstab', '/etc/fstab.bak')

        let file = cockpit.file('/etc/fstab', {superuser: 'require'});
        file.replace(output).then(tag => {
            console.log('Successfully wrote fstab', tag);
            addToast('Saved output to /etc/fstab');
        }).catch(err => {
            console.error('Error writing /etc/fstab', err);
            addToast(`Error saving to fstab: ${err.message}`, {appearance: 'error'});
        });

    }

    const toggleAutoUpdate = () => setAutoUpdate(!autoUpdate)
    const toggleBeautify = () => setBeautify(!beautify)

    String.prototype.padTabs = function(width) {
        if (this.length >= width)
            return this + '\t';

        let tabs = Math.ceil((width - this.length) / 8)
        if (tabs % 8 === 0)
            tabs++;
        return this + '\t'.repeat(tabs)
    }

    const genOutput = () => {
        // TODO: When verifying that its not empty, check if device is /tmp and no mountpoint specified (that will be considered an 'empty' row

        let output = '# device   mountpoint   fstype   options   dump   pass\n\n';

        for (let s of sections) {
            output += `#!# ${s.name}\n`
            for (let m of s.mounts) {
                if (m.device === '' || m.mountpoint === '')
                    continue;

                let line = ''
                if (beautify) {
                    if (m.device.length > maxBeautify) {
                        line += m.device + '\t\t';
                        line += m.mountpoint + '\t\t';
                        line += m.fstype + '\t\t';
                        line += m.options + '\t\t';
                        line += m.dump + '\t\t';
                        line += `${m.pass}\n`
                    }
                    else {
                        line += m.device.padTabs(maxBeautify);
                        line += m.mountpoint.padTabs(Math.floor(40));
                        line += m.fstype.padTabs(8);
                        line += m.options.padTabs(64);
                        line += m.dump.padTabs(8)
                        line += `${m.pass}\n`
                    }

                }
                else {
                    line += `${m.device} `
                    line += `${m.mountpoint} `
                    line += `${m.fstype} `
                    line += `${m.options} `
                    line += `${m.dump} `
                    line += `${m.pass}\n`
                }

                output += line
            }
            output += '\n\n'
        }
        setOutput(output);
        return output;
    }


    return (
        <PageContext.Provider value={{
            sections, setSections,
            parseFstab,
            genOutput,
            handleSave,
            toggleAutoUpdate,
            beautify, setBeautify,
            toggleBeautify,
            maxBeautify, setMaxBeautify,
            output, setOutput,
            handleAutoMount,
        }}>
            {children}
        </PageContext.Provider>
    );
}


export const useGlobal = () => {
    const ctx = useContext(PageContext);

    if (!ctx) {
        throw Error('The `useGlobal` hook must be called from a descendent of `PageProvider`.');
    }

    return {
        sections: ctx.sections,
        setSections: ctx.setSections,
        parseFstab: ctx.parseFstab,
        genOutput: ctx.genOutput,
        handleSave: ctx.handleSave,
        toggleAutoUpdate: ctx.toggleAutoUpdate,
        beautify: ctx.beautify,
        toggleBeautify: ctx.toggleBeautify,
        autoUpdate: ctx.autoUpdate,
        maxBeautify: ctx.maxBeautify,
        setMaxBeautify: ctx.setMaxBeautify,
        output: ctx.output,
        handleAutoMount: ctx.handleAutoMount,
    };
};
