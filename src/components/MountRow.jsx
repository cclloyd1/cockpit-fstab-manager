import {
    darken,
    lighten,
    makeStyles,
    TableRow,
} from "@material-ui/core";
import {useEffect, useState} from "react";
import MountRowEdit from "./MountRowEdit";
import MountRowView from "./MountRowView";
import clsx from "clsx";
import {Draggable} from "react-beautiful-dnd";


const useStyles = makeStyles(theme => ({
    dragging: {
        backgroundColor: lighten(theme.palette.background.paper, 0.07),
        boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
    },
    isHovering: {
        backgroundColor: darken(theme.palette.background.paper, 0.07),
    }
}));


export default function MountRow(props) {
    const classes = useStyles();

    const { mount, index, dragID, dragIndex } = props;

    const [state, setState] = useState({
        ...mount,
        options: mount.options.split(','),
    });

    const [edit, setEdit] = useState(false);

    const toggleEdit = () => {
        setEdit(!edit);
    }


    return (
        <Draggable draggableId={dragID} index={dragIndex}>
            {(provided, snapshot) => (
                <TableRow
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    className={clsx({
                        [classes.dragging]: snapshot.isDragging,
                        [classes.isHovering]: snapshot.isDraggingOver,
                    })}
                >
                    {edit && <MountRowEdit  state={state} setState={setState} toggleEdit={toggleEdit} {...props} />}
                    {!edit && <MountRowView state={state} setState={setState} toggleEdit={toggleEdit} {...props} />}
                </TableRow>
            )}
        </Draggable>
    );
}

