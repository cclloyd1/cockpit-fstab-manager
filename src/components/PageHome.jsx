import {
    Box,
    CircularProgress,
    Container,
    makeStyles, Typography
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";
import {useEffect, useState} from "react";
import {grey} from "@material-ui/core/colors";
import MountSection from "./MountSection";
import {DragDropContext} from "react-beautiful-dnd";
import { reorder } from "../utils";
import AddSection from "./AddSection";
import FileOutput from "./FileOutput";
import SaveArea from "./SaveArea";


const cockpit = window.cockpit;


const useStyles = makeStyles(theme => ({
    root: {
        minWidth: '100%',
        minHeight: '100%',
    },
    container: {
        height: '100%',
        padding: theme.spacing(2),
    },
    options: {
        padding: theme.spacing(0.5, 1),
        background: grey[700],
        borderRadius: theme.spacing(10),
        margin: theme.spacing(0.5, 0.25),
    },
    loadingBox: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(2),
        width: '100%',
        height: '100%',
    },
    loadingIcon: {
        color: theme.palette.text.primary,
    },
    infoText: {
        fontSize: theme.typography.pxToRem(12),
        marginBottom: theme.spacing(2),
    }
}));


export default function PageHome() {
    const classes = useStyles();

    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);

    const { sections } = useGlobal();
    const { setSections, parseFstab } = useGlobal();

    useEffect(() => {
        try {
            cockpit.file("/etc/fstab").read().then((content, tag) => {
                parseFstab(content);
                setLoading(false);
            }).catch(err => {
                console.error('Error reading fstab file.', err);
                setError(true);
                setLoading(false);
            })
        }
        catch {
            console.error('Cockpit not defined');
            //setError(true);
            //setLoading(false);
            const fstab = "# device\t\t\t\t\tmountpoint\t\tfstype\toptions\t\t\t\t\t\t\t\tdump\t\tpass\n" +
                "UUID=75ed3ae8-5c0a-4faf-93eb-00d1a7c31591 / ext4 defaults 0 0\n" +
                "UUID=9cc5d260-f35a-4566-94f4-df1b000849a6 /boot ext4 defaults 0 0\n" +
                "/swap.img\tnone\tswap\tsw\t0\t0\n" +
                "\n" +
                "UUID=54d8ed0e-ec4c-46f2-9840-b9e286009847\t/mnt/kubenfs/fast\text4\tdefaults\t0\t0\n" +
                "\n" +
                "UUID=cd687f33-40ae-4a63-a60e-6126d34a746c\t/mnt/Hyperspace\t\tbtrfs\tdefaults\t0\t0\n" +
                "UUID=1f6d149d-ff1b-4fe7-b344-6f79d5349b81\t/mnt/Phobos\t\tbtrfs\tdefaults\t0\t0\n" +
                "10.10.10.2:/\t\t\t\t\t/mnt/skipspace\t\tceph\tname=admin,secretfile=/etc/ceph/admin.key,noatime,_netdev\t0\t0\n" +
                "/mnt/skipspace\t\t\t\t\t/srv/Skipspace\t\tnone\tbind,defaults,nofail\t\t\t\t\t\t0\t\t0\n" +
                "\n" +
                "/Ultraspace/Home\t\t\t\t/mnt/Home\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "\n" +
                "\n" +
                "/mnt/kubenfs/fast/casualtryhards-foundryvtt-config-pvc-09d83209-4a78-432c-9eb8-f124204928de/Data\t/srv/Foundry\tnone\tbind,defaults\t0\t0\n" +
                "/srv/Foundry\t\t\t/mnt/Home/foundry/Foundry\tnone\tbind,defaults\t0\t0\n" +
                "/mnt/Hyperspace/Storage/DND\t/mnt/Home/foundry/DND\t\tnone\tbind,defaults\t0\t0\n" +
                "/mnt/Home/foundry\t\t/secure/jail/foundry/mnt/Home/foundry\t\tnone\tbind,defaults\t0\t0\n" +
                "\n" +
                "# Backups\n" +
                "/mnt/kubenfs/fast\t\t\t/srv/K8sFast\t\tnone\tbind,defaults,nofail\t\t\t\t\t\t0\t\t0\n" +
                "/mnt/Hyperspace/Backups/VM\t\t\t/srv/VMBackups\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Backups/Ceph\t\t/srv/Ceph\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Backups/TimeMachine\t\t/srv/TimeMachine\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "\n" +
                "\n" +
                "# Infrastructure\n" +
                "/mnt/Hyperspace/Datastores\t\t\t/srv/Datastores\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Datastores/ISO\t\t/srv/ISO\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Datastores/Web\t\t/srv/Web\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "\n" +
                "\n" +
                "# Media\n" +
                "/mnt/Hyperspace/Media\t\t\t/srv/Media\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/Ultraspace/Downloads\t\t\t/srv/Downloads\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/Movies/Movies\t\t/srv/Movies\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/Movies/4k\t\t/srv/Movies4k\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/Movies/BluRay\t\t/srv/MoviesBluRay\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/TV/TV\t\t\t/srv/TV\t\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/TV/4k\t\t\t/srv/TV4k\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/TV/BluRay\t\t/srv/TVBluRay\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/Music/Music\t\t/srv/Music\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/Anime/Subbed\t\t/srv/Anime\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "/mnt/Hyperspace/Media/Anime/Dubbed\t\t/srv/AnimeDub\t\tnone\tbind,defaults,nofail,x-systemd.requires=zfs-mount.service\t0\t\t0\n" +
                "\n"

            parseFstab(fstab)
            setLoading(false);
        }
    }, []);


    const onDragEnd = (result) => {
        // dropped outside the list
        if (!result.destination) {
            console.warn('outside list')
            return
        }

        // Reorder within same section
        if (result.source?.droppableId === result.destination?.droppableId) {
            const sectionIndex = parseInt(result.source.droppableId)
            let section = sections[sectionIndex]

            const items = reorder(
                section.mounts,
                result.source.index,
                result.destination.index
            )

            const newSections = [...sections]
            newSections[sectionIndex] = {
                ...section,
                mounts: items,
            }
            setSections(newSections)
        }
        // Move to other section
        else {
            const sourceIndex = parseInt(result.source.droppableId)
            const destIndex = parseInt(result.destination.droppableId)

            console.log(sourceIndex, destIndex, result.source.index, result.destination.index)

            // Remove from old section
            const sourceSection = sections[sourceIndex]
            const sourceItem = {...sections[sourceIndex].mounts[result.source.index]}
            sourceSection.mounts.splice(result.source.index, 1)

            // Insert into new section
            const destSection = sections[destIndex]
            destSection.mounts.splice(result.destination.index, 0, sourceItem)

            // Set reordered arrays to state
            const newSections = [...sections]
            newSections[sourceIndex] = sourceSection;
            newSections[destIndex] = destSection;
            setSections([...newSections])
        }
    }



    return (
        <Box className={classes.root}>
            <Container maxWidth={'lg'} className={classes.container}>
                {loading && <Box className={classes.loadingBox}>
                    <CircularProgress size={50} className={classes.loadingIcon} />
                </Box>}


                {!loading && !error && <>
                    <SaveArea/>

                    <Typography variant={'h4'}>Sections</Typography>
                    <Typography className={classes.infoText}>(click to expand each section, drag mounts to reorder or move between sections)</Typography>
                    <DragDropContext onDragEnd={onDragEnd}>
                        {sections.map((s, i) => <MountSection section={s} index={i} key={i} dragend={onDragEnd} defaultExpanded={sections.length === 1}/>)}
                        <AddSection/>
                    </DragDropContext>

                    <FileOutput/>
                </>}
            </Container>
        </Box>
    );
}

