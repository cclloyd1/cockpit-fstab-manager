import {
    Accordion,
    AccordionDetails,
    AccordionSummary, Button, darken, IconButton, lighten,
    makeStyles, Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow, TextField, Tooltip, Typography
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";
import MountRow from "./MountRow";
import {DragDropContext, Droppable} from "react-beautiful-dnd";
import {useEffect, useState} from "react";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import SaveIcon from "@material-ui/icons/Save";
import EditIcon from "@material-ui/icons/Edit";


const DroppableComponent = (onDragEnd, id) => ({children, ...otherProps}) => {
    return (
            <Droppable droppableId={`${id}`} direction="vertical">
                {(provided) => {
                    return (
                        <TableBody ref={provided.innerRef} {...provided.droppableProps} {...otherProps}>
                            {children}
                            {provided.placeholder}
                        </TableBody>
                    )
                }}
            </Droppable>
    )
}


const useStyles = makeStyles(theme => ({
    sectionTitle: {
        display: 'flex',
        alignItems: 'center',
    }
}));


export default function MountSection(props) {
    const classes = useStyles();

    const { sections } = useGlobal();
    const { setSections } = useGlobal();

    const { section, index, dragend, defaultExpanded } = props;

    const [hovering, setHovering] = useState(false);

    const addMount = () => {
        const newSections = [...sections];
        newSections[index] = {
            ...section,
            mounts: [
                ...section.mounts,
                {
                    device: '/tmp',
                    mountpoint: '',
                    fstype: 'none',
                    options: 'defaults',
                    pass: '0',
                    dump: '0',
                }
            ]
        };
        setSections([...newSections]);
    }

    const [expanded, setExpanded] = useState(defaultExpanded);
    const [edit, setEdit] = useState(false);
    const [name, setName] = useState(section.name);

    const handleChange = (event) => {
        setName(event.target.value);
    }

    const handleSave = () => {
        const newSection = sections;
        newSection[index].name = name;
        setSections([...newSection]);
        setEdit(!edit);
    }

    const toggleExpanded = (event) => {
        if (event.target.tagName.toLowerCase() === 'div' || event.target.tagName.toLowerCase() === 'p')
            setExpanded(!expanded)
    }

    return (
        <Accordion TransitionProps={{ unmountOnExit: true }} expanded={expanded}>
            <AccordionSummary className={classes.sectionTitle} onClick={toggleExpanded}>
                {!edit && <Typography>{name} <IconButton size={'small'} onClick={() => setEdit(!edit)}><EditIcon/></IconButton></Typography>}
                {edit && <TextField
                    size={'small'}
                    variant={'outlined'}
                    label={'Section Name'}
                    value={name}
                    onChange={handleChange}
                    InputProps={{
                        endAdornment: <SaveIcon onClick={handleSave}/>
                    }}
                />}
            </AccordionSummary>
            <AccordionDetails>
                <Table size={'small'} style={{ tableLayout: 'fixed' }}>
                    <colgroup>
                        <col style={{width:'3%'}}/>
                        <col style={{width:'28%'}}/>
                        <col style={{width:'15%'}}/>
                        <col style={{width:'8%'}}/>
                        <col style={{width:'34%'}}/>
                        <col style={{width:'5%'}}/>
                        <col style={{width:'5%'}}/>
                    </colgroup>
                    <TableHead>
                        <TableRow>
                            <TableCell/>
                            <TableCell>Device</TableCell>
                            <TableCell>Mount Point</TableCell>
                            <TableCell>FS Type</TableCell>
                            <TableCell>Options</TableCell>
                            <TableCell>Dump</TableCell>
                            <TableCell>Pass</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody component={DroppableComponent(dragend, index)}>
                        {section.mounts.map((m, i) =>
                            <MountRow
                                key={i}
                                mount={m}
                                index={i}
                                dragID={m.device}
                                dragIndex={i}
                            />)}
                            <TableRow>
                                <TableCell colSpan={7} align={'center'}>
                                    <Button onClick={addMount} startIcon={<AddCircleIcon/>}>Add Device</Button>
                                </TableCell>
                            </TableRow>

                    </TableBody>
                </Table>
            </AccordionDetails>
        </Accordion>
    );
}

