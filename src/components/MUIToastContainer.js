import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { DefaultToastContainer } from 'react-toast-notifications';


const useStyles = makeStyles(theme => ({
    root: {
        zIndex: '5000 !important',
        paddingTop: theme.mixins.toolbar.minHeight,
    }
}));

export default function MUIToastContainer(props) {
    const classes = useStyles();
    const { children } = props;

    return (
        <DefaultToastContainer {...props} className={classes.root} >
            {children}
        </DefaultToastContainer>
    );
}

MUIToastContainer.defaultProps = {

};
