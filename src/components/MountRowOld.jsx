/*global asdf*/
import {
    Box,
    CircularProgress,
    Container, FormControl, IconButton, InputAdornment,
    makeStyles, Select, Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow, TextField, Typography
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";
import {useEffect, useState} from "react";
import {grey} from "@material-ui/core/colors";
import SaveIcon from '@material-ui/icons/Save';


const useStyles = makeStyles(theme => ({
    options: {
        padding: theme.spacing(0.5, 1),
        background: grey[700],
        borderRadius: theme.spacing(10),
        margin: theme.spacing(0.5, 0.25),
        wordBreak: 'keep-all',
        whiteSpace: 'nowrap',
    },
}));


export default function MountRow(props) {
    const classes = useStyles();

    const { mount, index } = props;

    const { sections } = useGlobal();
    const { setSections } = useGlobal();

    const [state, setState] = useState({
        ...mount,
        options: mount.options.split(','),
    });

    const defaultEdit = {...mount};
    for (const property in defaultEdit) {
        defaultEdit[property] = false;
    }
    const [edit, setEdit] = useState(defaultEdit);

    const handleSelectChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value,
        })
    };

    const handleTextChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value,
        });
    };

    const toggleEdit = device => {
        setEdit({
            ...edit,
            [device]: !edit[device],
        })
    }


    return (
        <>
            <TableRow>
                <TableCell>
                    {!edit.device && <Typography onClick={() => toggleEdit('device')}>{state.device}</Typography>}
                    {edit.device && <TextField
                        fullWidth
                        value={state.device}
                        name={'device'}
                        variant={'filled'}
                        size={'small'}
                        label={'Device'}
                        onChange={handleTextChange}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                                <IconButton onClick={() => toggleEdit('device')}>
                                    <SaveIcon/>
                                </IconButton>
                            </InputAdornment>,
                        }}
                    />}
                </TableCell>
                <TableCell>
                    {!edit.mountpoint && <Typography onClick={() => toggleEdit('mountpoint')}>{state.mountpoint}</Typography>}
                    {edit.mountpoint && <TextField
                        fullWidth
                        value={state.mountpoint}
                        name={'mountpoint'}
                        variant={'filled'}
                        size={'small'}
                        label={'Mount Point'}
                        onChange={handleTextChange}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                                <IconButton onClick={() => toggleEdit('mountpoint')}>
                                    <SaveIcon/>
                                </IconButton>
                            </InputAdornment>,
                        }}
                    />}
                </TableCell>
                <TableCell>
                    {!edit.fstype && <Typography onClick={() => toggleEdit('fstype')}>{state.fstype}</Typography>}
                    {edit.device && <TextField
                        fullWidth
                        value={state.fstype}
                        name={'fstype'}
                        variant={'filled'}
                        size={'small'}
                        label={'FS Type'}
                        onChange={handleTextChange}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                                <IconButton onClick={() => toggleEdit('fstype')}>
                                    <SaveIcon/>
                                </IconButton>
                            </InputAdornment>,
                        }}
                    />}
                </TableCell>
                <TableCell>
                    {state.options.map(o =>
                        <span className={classes.options} key={o}>{o}</span>
                    )}
                </TableCell>
                <TableCell>
                    <FormControl className={classes.formControl}>
                        <Select
                            native
                            value={state.dump}
                            defaultValue={0}
                            onChange={handleSelectChange}
                            inputProps={{
                                name: 'pass',
                            }}
                        >
                            <option value={0}>0</option>
                            <option value={1}>1</option>
                        </Select>
                    </FormControl>
                </TableCell>
                <TableCell>
                    <FormControl className={classes.formControl}>
                        <Select
                            native
                            value={state.pass}
                            defaultValue={0}
                            onChange={handleSelectChange}
                            inputProps={{
                                name: 'pass',
                            }}
                        >
                            <option value={0}>0</option>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                        </Select>
                    </FormControl>
                </TableCell>
            </TableRow>
        </>
    );
}

