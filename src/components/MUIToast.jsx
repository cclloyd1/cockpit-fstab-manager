import {
    makeStyles
} from "@material-ui/core";
import Alert from '@material-ui/lab/Alert';


const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(1),
    },
}));


export default function MUIToast(props) {
    const classes = useStyles();
    const { children, appearance, ...otherProps } = props;

    return (
        <Alert severity={appearance} variant={'filled'} elevation={3} className={classes.root}>
            {children}
        </Alert>
    );
}

MUIToast.defaultProps = {
    appearance: 'success',
}

