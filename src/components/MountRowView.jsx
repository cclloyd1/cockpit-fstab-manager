import {IconButton,
    makeStyles,
    TableCell,Typography
} from "@material-ui/core";
import {grey} from "@material-ui/core/colors";
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles(theme => ({
    option: {
        display: 'inline-block',
        padding: theme.spacing(0.5, 1),
        background: grey[700],
        borderRadius: theme.spacing(10),
        margin: theme.spacing(0.25, 0.25),
        wordBreak: 'keep-all',
        whiteSpace: 'nowrap',
        boxShadow: '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
    },
}));


export default function MountRowView(props) {
    const classes = useStyles();

    const { mount, index, state, toggleEdit } = props;

    return (
        <>
            <TableCell style={{width:'3%'}}>
                <IconButton onClick={toggleEdit} size={'small'}><EditIcon/></IconButton>
            </TableCell>

            <TableCell style={{width:'28%'}}>
                <Typography>
                    {state.device}
                </Typography>
            </TableCell>

            <TableCell style={{width:'15%'}}>
                <Typography>{state.mountpoint}</Typography>
            </TableCell>

            <TableCell style={{width:'8%'}}>
                <Typography>{state.fstype}</Typography>
            </TableCell>

            <TableCell style={{width:'34%'}}>
                {state.options.map(o =>
                    <span className={classes.option} key={o}>{o}</span>
                )}
            </TableCell>

            <TableCell style={{width:'5%'}}>
                <Typography>{state.dump}</Typography>
            </TableCell>

            <TableCell style={{width:'5%'}}>
                <Typography>{state.pass}</Typography>
            </TableCell>
        </>
    );
}

