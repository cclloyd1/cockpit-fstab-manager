import {
    Box, Checkbox,
    CircularProgress,
    Container, darken, Divider, FormControl, FormControlLabel, Grid, IconButton, InputAdornment,
    makeStyles, Paper, Select, Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow, TextField, Tooltip, Typography
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";
import {useEffect, useState} from "react";
import {grey} from "@material-ui/core/colors";
import SaveIcon from '@material-ui/icons/Save';
import EditIcon from '@material-ui/icons/Edit';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles(theme => ({
    edit: {
        backgroundColor: darken(theme.palette.background.paper, 0.15),
        padding: theme.spacing(3, 1),
    },
    editOptionsBox: {
        borderRadius: theme.spacing(1),
        borderColor: theme.palette.text.primary,
        borderWidth: 1,
        padding: theme.spacing(2),
    },
    commonOption: {
        borderRadius: theme.spacing(10),
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(0, 1.5, 0, 0.5),
        margin: theme.spacing(1, 0.5),
    },
    customOption: {
        margin: theme.spacing(1, 0.5),
    },
    allCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    optionBox: {
        position: 'relative',
    },
    optionDeleteButton: {
        position: 'absolute',
        top: 0,
        right: 0,
        color: theme.palette.error.dark,
    },
    tooltip: {
        fontSize: theme.typography.pxToRem(10),
        backgroundOpacity: 1,
    },
}));

const common = [
    {
        name: 'defaults',
        description: 'Uses the defaults: rw, suid, dev, exec, auto, nouser, and async',
    }, {
        name: 'bind',
        description: 'Mount a directory to another directory',
    }, {
        name: 'nofail',
        description: 'Do not report errors for this device if it does not exist.',
    }, {
        name: 'noatime',
        description: 'Do not update inode access time',
    }, {
        name: '_netdev',
        description: 'The filesystem resides on a device that requires network access',
    }, {
        name: 'remount',
        description: 'Attempt to remount directory with set options.  Works differently with bind mounts.',
    }, {
        name: 'ro',
        description: 'Read-only',
    },
]

const findWithAttr = (array, attr, value) => {
    for(let i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

const includesWithAttr = (array, attr, value) => {
    for(let i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return true;
        }
    }
    return false;
}

export default function MountRowEdit(props) {
    const classes = useStyles();

    const { mount, index, state, setState, toggleEdit } = props;

    const handleTextChange = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value,
        });
    };

    const handleOptionChange = (event) => {
        const index = event.target.dataset.option;
        state.options[index] = event.target.value
        setState({
            ...state,
            options: [...state.options],
        });
    }

    const handleCommonChange = (event) => {
        const option = event.target.dataset.option;

        if (state.options.indexOf(option) === -1) {
            setState({
                ...state,
                options: [...state.options, option]
            })
        }
        else {
            let newOptions = [...state.options];
            newOptions.splice(state.options.indexOf(option), 1)
            setState({
                ...state,
                options: [...newOptions]
            })
        }
    }

    const addOption = () => {
        setState({
            ...state,
            options: [...state.options, '']
        })
    }

    const handleRemoveOption = (index) => {
        console.log(index);
        let newOptions = [...state.options]
        newOptions.splice(index, 1)
        setState({
            ...state,
            options: [...newOptions],
        })
    }


    return (
        <TableCell colSpan={7} className={classes.edit}>
            <Grid container spacing={1}>

                <Grid item xs={1} className={classes.allCenter}>
                    <IconButton onClick={toggleEdit}><SaveIcon/></IconButton>
                </Grid>


                <Grid item container spacing={1} xs={11}>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            name={'device'}
                            label={'Device'}
                            variant={'filled'}
                            size={'small'}
                            onChange={handleTextChange}
                            value={state.device}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <TextField
                            fullWidth
                            name={'mountpoint'}
                            label={'Mount Point'}
                            variant={'filled'}
                            size={'small'}
                            onChange={handleTextChange}
                            value={state.mountpoint}
                        />
                    </Grid>
                    <Grid item xs={2}>
                        <TextField
                            fullWidth
                            name={'fstype'}
                            label={'Filesystem Type'}
                            variant={'filled'}
                            size={'small'}
                            onChange={handleTextChange}
                            value={state.fstype}
                        />
                    </Grid>
                    <Grid item xs={1}>
                        <TextField
                            fullWidth
                            select
                            name={'dump'}
                            label={'Dump'}
                            variant={'filled'}
                            size={'small'}
                            onChange={handleTextChange}
                            value={state.dump}
                            SelectProps={{
                                native: true,
                            }}
                        >
                            <option value={0}>0</option>
                            <option value={1}>1</option>
                        </TextField>
                    </Grid>
                    <Grid item xs={1}>
                        <TextField
                            fullWidth
                            select
                            name={'pass'}
                            label={'Pass'}
                            variant={'filled'}
                            size={'small'}
                            onChange={handleTextChange}
                            value={state.pass}
                            SelectProps={{
                                native: true,
                            }}
                        >
                            <option value={0}>0</option>
                            <option value={1}>1</option>
                            <option value={2}>2</option>
                        </TextField>
                    </Grid>



                    <Grid item xs={12}>
                        <fieldset className={classes.editOptionsBox}>
                            <legend><Typography>Options</Typography></legend>
                            <Table style={{ tableLayout: 'fixed' }}>
                                <colgroup>
                                    <col style={{width:'10%'}}/>
                                    <col style={{width:'90%'}}/>
                                </colgroup>
                                <TableBody>
                                    <TableRow>
                                        <TableCell variant={'head'} align={'right'}>Common</TableCell>
                                        <TableCell>
                                            {common.map((c, i) =>
                                                <Tooltip className={classes.tooltip}
                                                    title={<Typography>{c.description}</Typography>}
                                                    arrow
                                                ><FormControlLabel
                                                    key={c.name}
                                                    label={c.name}
                                                    className={classes.commonOption}
                                                    control={
                                                        <Checkbox
                                                            checked={state.options.includes(c.name)}
                                                            onChange={handleCommonChange}
                                                            name={`checked-${c.name}`}
                                                            inputProps={{'data-option': c.name}}
                                                        />
                                                    }/>
                                                </Tooltip>
                                            )}
                                        </TableCell>
                                    </TableRow>
                                    <TableRow>
                                        <TableCell variant={'head'} align={'right'}>Custom</TableCell>
                                        <TableCell>
                                            <Grid container spacing={2}>
                                            {state.options.map((o, i) => {
                                                if (!includesWithAttr(common, 'name', o))
                                                    return <Grid item xs={4} key={i} className={classes.optionBox}>
                                                        <TextField
                                                            fullWidth
                                                            label={`Custom Option ${i}`}
                                                            name={`options[${i}]`}
                                                            variant={'filled'}
                                                            size={'small'}
                                                            value={state.options[i]}
                                                            inputProps={{'data-option': i}}
                                                            onChange={handleOptionChange}
                                                            style={{minWidth: 300}}
                                                            InputProps={{
                                                                endAdornment:
                                                                    <InputAdornment position={'end'}>
                                                                        <Tooltip title={'Remove this option'} arrow>
                                                                            <IconButton
                                                                                size={'small'}
                                                                                onClick={() => handleRemoveOption(i)}
                                                                                edge={'end'}
                                                                            ><DeleteIcon/></IconButton>
                                                                        </Tooltip>
                                                                     </InputAdornment>
                                                            }}
                                                        />
                                                    </Grid>
                                            })}

                                            <Grid item xs={1} className={classes.allCenter}>
                                                <Tooltip title={'Add a custom option'} arrow>
                                                    <IconButton onClick={addOption}><AddCircleIcon/></IconButton>
                                                </Tooltip>
                                            </Grid>
                                            </Grid>
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </fieldset>
                    </Grid>


                </Grid>

            </Grid>
        </TableCell>
    );
}

