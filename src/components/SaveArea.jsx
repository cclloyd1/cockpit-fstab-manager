import {
    Box, Button, FormControlLabel, lighten,
    makeStyles, Paper, Switch, TextField, Typography
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";
import SaveIcon from "@material-ui/icons/Save";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2, 2),
        margin: theme.spacing(2, 0),
    },
    buttons: {
        '& > *': {
            marginRight: theme.spacing(2),
        }
    },
    switch: {
        backgroundColor: lighten(theme.palette.background.paper, 0.15),
        display: 'inline-block',
        borderRadius: theme.spacing(10),
        padding: theme.spacing(0, 0, 0, 1.5),
        marginLeft: theme.spacing(1),
    },
}));


export default function SaveArea() {
    const classes = useStyles();

    const { autoUpdate, beautify, maxBeautify } = useGlobal();
    const { handleSave, toggleAutoUpdate, toggleBeautify, setMaxBeautify, handleAutoMount } = useGlobal();

    const handleMaxBeautifyChange = (e) => {
        setMaxBeautify(e.target.value);
    }

    return (
        <Paper className={classes.root}>
            <Typography variant={'h5'} gutterBottom>Controls</Typography>
            <Box className={classes.buttons}>
                <Button
                    style={{textTransform: 'none'}}
                    onClick={handleSave}
                    startIcon={<SaveIcon/>}
                    variant={'contained'}
                    color={'primary'}
                >
                    SAVE TO /etc/fstab
                </Button>
                <Paper className={classes.switch}><FormControlLabel
                    label='Beautify Output'
                    control={
                        <Switch
                            checked={beautify}
                            onChange={toggleBeautify}
                        />
                    }
                /></Paper>
                <TextField
                    type={'number'}
                    value={maxBeautify}
                    onChange={handleMaxBeautifyChange}
                    variant={'outlined'}
                    label={'Line Width'}
                    size={'small'}
                    style={{ width: 100 }}
                />
                <Paper className={classes.switch}><FormControlLabel
                    label='Auto-Update Output'
                    control={
                        <Switch
                            checked={autoUpdate}
                            onChange={toggleAutoUpdate}
                        />
                    }
                /></Paper>
                <Button
                    onClick={handleAutoMount}
                    variant={'contained'}
                    color={'primary'}
                    style={{textTransform: 'none'}}
                    disabled
                >
                    RUN `mount -a`
                </Button>
            </Box>
        </Paper>
    );
}

