import {
    Box, Button,
    CircularProgress,
    Container, IconButton,
    makeStyles, Tooltip
} from "@material-ui/core";
import {useGlobal} from "./PageProvider";
import {useEffect, useState} from "react";
import {grey} from "@material-ui/core/colors";
import MountSection from "./MountSection";
import {DragDropContext} from "react-beautiful-dnd";
import { reorder } from "../utils";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import {Add} from "@material-ui/icons";


const cockpit = window.cockpit;


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(2),
    },
    button: {
        //padding: theme.spacing(1),
    },
    icon: {
        width: 50,
        height: 50,
    },
}));


export default function AddSection() {
    const classes = useStyles();

    const { sections } = useGlobal();
    const { setSections } = useGlobal();

    const addSection = () => {
        setSections([
            ...sections,
            {
                name: 'New Section',
                mounts: [],
            }
        ])
    }


    return (
        <Box className={classes.root}>
            <Tooltip title={'Add new section'}>
                <Button onClick={addSection} className={classes.button} startIcon={<AddCircleIcon/>} size={'large'}>Add Section</Button>
            </Tooltip>
        </Box>
    );
}

