import { unstable_createMuiStrictModeTheme as createMuiTheme } from '@material-ui/core';
//import { createMuiTheme } from '@material-ui/core/styles';

// A custom theme for this app
const theme = createMuiTheme({
    palette: {
        type: 'dark',
        primary: {
            main: '#0099d3',
        },
        secondary: {
            main: '#39a5dc',
        },
    },
});

export default theme;