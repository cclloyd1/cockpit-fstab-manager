import theme from "./theme";
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import PageHome from "./components/PageHome";
import PageProvider from "./components/PageProvider";
import { ToastProvider, useToasts } from 'react-toast-notifications';
import MUIToast from "./components/MUIToast";
import MUIToastContainer from "./components/MUIToastContainer";

function App() {
    return (
        <ThemeProvider theme={theme}>
            <ToastProvider autoDismissTimeout={5000} autoDismiss={true} placement="top-center" components={{ ToastContainer: MUIToastContainer, Toast: MUIToast }}>
                <PageProvider>
                        <CssBaseline />
                        <PageHome/>
                </PageProvider>
            </ToastProvider>
        </ThemeProvider>
    );
}
export default App;


// https://example.com:9090/cockpit/@localhost/cockpit-fstab-manager/cockpit-base/app.html
