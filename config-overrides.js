const path = require('path');
const fs = require('fs');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
    webpack: (config, env) => {
        if (env === 'development') {
            config = {
                ...config,
                output: {
                    ...config.output,
                    filename: 'bundle.js',
                    path: resolveApp('app'),
                    chunkFilename: '[name].chunk.js',
                    publicPath: '',
                },
                optimization: {
                    ...config.optimization,
                    splitChunks: {
                        chunks: 'all',
                        name: false
                    },
                },
                plugins: [
                    ...config.plugins,
                    /*
                    new HtmlWebpackPlugin({
                        filename: 'index.html',
                        template: 'public/index_dev.html',
                        chunks: ['main'],
                    }),*/
                ]
            }
            return config;
        }
        config = {
            ...config,
            output: {
                ...config.output,
                //filename: 'bundle.js',
                //path: resolveApp('app'),
                //chunkFilename: '[name].chunk.js',
                publicPath: './',
            },
            optimization: {
                ...config.optimization,
                splitChunks: {
                    chunks: 'all',
                    name: false
                },
            },

        }
        return config;
    },
    devServer: (configFunction) => {
        return function(proxy, allowedHost) {
            const config = configFunction(proxy, allowedHost);
            config.writeToDisk = true;
            return config;
        };
    }
}
